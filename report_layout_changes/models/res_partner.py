# -*- coding: utf-8 -*-

from flectra import models, fields, api,_

class ResPartner(models.Model):
    _inherit = 'res.partner'

    eq_customer_ref = fields.Char("Customer Number")
    eq_creditor_ref = fields.Char('Supplier Number')
    eq_house_no = fields.Char("House number")
    eq_firstname = fields.Char('Firstname')
    eq_name2 = fields.Char('Name2')
    eq_name3 = fields.Char('Name3')
    eq_citypart = fields.Char('Disctirct')
    
    
    @api.multi
    def eq_customer_update(self):
        #Gets the Partner
        #If the field isn't filled, it should do this
        if not self.eq_customer_ref:
                #Gets the sequence and sets it in the apropriate field
                ref = self.env['ir.sequence'].next_by_code('eq_customer_ref')
                vals = {
                    'eq_customer_ref': ref,
                    'ref': ref,
                }

                super(ResPartner, self).write(vals)
                
