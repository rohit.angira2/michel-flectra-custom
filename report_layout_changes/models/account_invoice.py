# -*- coding: utf-8 -*-

from flectra import models, fields, api,_
from datetime import datetime


def grouplines(self, ordered_lines, sortkey):
    """Return lines from a specified invoice or sale order grouped by category"""

    grouped_lines = []
    group_ids = []
    for line in ordered_lines:
        if (line.sale_layout_cat_id.id in group_ids):
            for group_line in grouped_lines:
                if group_line['category'] == line.sale_layout_cat_id:
                    group_line['subtotal'] += line.price_subtotal
                    group_line['lines'].append(line)
        else:
            group = {}
            group['category'] = line.sale_layout_cat_id
            group['subtotal'] = line.price_subtotal
            group['lines'] = []
            group['lines'].append(line)
            grouped_lines.append(group)
            group_ids.append(line.sale_layout_cat_id.id)

    return grouped_lines

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    
    eq_ref_number = fields.Char('Sale Order Referenc')
    eq_gutschrift = fields.Boolean(string="Ist eine Gutschrift")
    eq_head_text = fields.Html('Head Text')
    eq_use_page_break_after_header = fields.Boolean('Page break after header text')
    eq_use_page_break_before_footer = fields.Boolean('Page break before footer text')
    eq_delivery_address = fields.Many2one('res.partner','Delivery Address')
    state = fields.Selection([
            ('draft','Draft'),
            ('proforma','Pro-forma'),
            ('proforma2','Pro-forma'),
            ('open', 'Open'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)
    
    
    def check_if_display_gross_price(self):
        """
            Check if we should display gross price
            @object: order position
            @return: True = display gross price
        """
        return self.env["eq_report_helper"].check_if_display_gross_price(self)
                
    """
    def check_if_display_gross_price(self, object):
        self.display_gross_price =  self.pool.get("eq_report_helper").check_if_display_gross_price(self.cr, self.uid, object)
        return self.display_gross_price
    """
    
    def sale_layout_lines(self, invoice_id=None):
        """
        Returns order lines from a specified sale ordered by
        sale_layout_category sequence. Used in sale_layout module.

        :Parameters:
            -'order_id' (int): specify the concerned sale order.
        """
        ordered_lines = self.browse(invoice_id).invoice_line_ids

        sortkey = lambda x: x.sale_layout_cat_id.id if x.sale_layout_cat_id.id else ''

        return grouplines(self, ordered_lines, sortkey)
    
    def get_qty(self, uom, language):
        return self.env["eq_report_helper"].get_qty(uom, language, 'Sale Quantity Report')
    
    def get_price(self, price, language, currency_id):                
        return self.env["eq_report_helper"].get_price(price, language, 'Sale Price Report', currency_id)
    
    def get_standard_price(self, price, language, currency_id):
        return self.env["eq_report_helper"].get_standard_price(price, language, currency_id)
    
    
    def get_gross_price_invoice(self, object, language, currency_id):
        """
            Get gross price for an invoice position as string together with currency
            @object: Invoice position
            @language: Language
            @currency_id: Currency
            @return: Gross  price of an invoice position as string together with currency
        """
        return self.env["eq_report_helper"].get_gross_price_invoice(object, language, currency_id)
    
    def get_gross_price_as_float_invoice(self, object, language, currency_id):
        """
            Get gross price for an invoice position as string together with currency
            @object: Invoice position
            @language: Language
            @currency_id: Currency
            @return: Gross  price of an invoice position as float
        """
        return self.env["eq_report_helper"].get_gross_price_as_float_invoice(object, language, currency_id)
    
    
    def calculate_sum(self, input, category_no, lines):
        """
            Calculate total price as sum of each position
            @input: input data
            @category_no: category
            @lines: all invoice lines of actual invoice
            @return: calculated sum
        """
        
        result = 0                
        for line in lines:
            #if line.eq_optional is False:                # this field is only on sale_order relevant
            #quantity = line.product_uom_qty              # the field product_uom_qty doesn't exist in account_invoice_line
            quantity = line.quantity
            price = line.price_unit
            result += quantity * price  

        return result
    
    def calculate(self, input, category_no):
        """
            Calculate total price without optional products
            @input: Subtotal price - total inkl. optional products
            @category_no: Category no
            @return: Total price without optional products
        """        
        
#         if category_no in self.data_dict:               # check if we find total price for category
#             if self.display_gross_price is False:
#                 total_price = self.data_dict[category_no]
#                 result = input - total_price
#                 return result
#             else:
#                 total_price = self.data_dict[category_no]            
#                 result = input - total_price
#                 return result
        
        return input
    
    def get_eq_payment_terms(self, object, language, currency_id):
        """
            Show payment terms with custom text using 2 kinds of placeholders.
            Date1 & Date2 = Placeholder for Date that will be calculated and replaced
            Value1 % Value2 = Placehold for Value that will be calculated and replaced            
            @object: account.invoice object
            @language: actual language
            @currency_id: actual currency_id of given invoice
            @return: Return new string with formated & calculated date and prices            
        """
        
        return self.env["eq_report_helper"].get_eq_payment_terms(object, language, currency_id)
    
    
class InvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    
    sale_layout_cat_id = fields.Many2one('sale_layout.category',string='Section')
    eq_move_id = fields.Many2one('stock.move')
    
    