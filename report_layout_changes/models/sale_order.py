# -*- coding: utf-8 -*-

from flectra import models, fields, api,_
from datetime import datetime

def grouplines(self, ordered_lines, sortkey):
    """Return lines from a specified invoice or sale order grouped by category"""

    grouped_lines = []
    group_ids = []
    for line in ordered_lines:
        if (line.sale_layout_cat_id.id in group_ids):
            for group_line in grouped_lines:
                if group_line['category'] == line.sale_layout_cat_id:
                    group_line['subtotal'] += line.price_subtotal
                    group_line['lines'].append(line)
        else:
            group = {}
            group['category'] = line.sale_layout_cat_id
            group['subtotal'] = line.price_subtotal
            group['lines'] = []
            group['lines'].append(line)
            grouped_lines.append(group)
            group_ids.append(line.sale_layout_cat_id.id)

    return grouped_lines

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    client_order_ref = fields.Char('Reference/Description',copy=True)
    eq_head_text = fields.Html('Head Text')
    eq_use_page_break_after_header = fields.Boolean('Page break after header text')
    eq_use_page_break_before_footer = fields.Boolean('Page break before footer text')
    show_delivery_date = fields.Boolean('Show Delivery Date')
    
    
    def check_if_display_gross_price(self):
        """
            Check if we should display gross price
            @object: order position
            @return: True = display gross price
        """
        return self.env["eq_report_helper"].check_if_display_gross_price(self)
                
    """
    def check_if_display_gross_price(self, object):
        self.display_gross_price =  self.pool.get("eq_report_helper").check_if_display_gross_price(self.cr, self.uid, object)
        return self.display_gross_price
    """
    
    
    def sale_layout_lines(self, order_id=None):
        """
        Returns order lines from a specified sale ordered by
        sale_layout_category sequence. Used in sale_layout module.

        :Parameters:
            -'order_id' (int): specify the concerned sale order.
        """
        ordered_lines = self.browse(order_id).order_line

        ordered_lines = sorted(ordered_lines, key=lambda x: x.sequence, reverse=False)

        sortkey = lambda x: x.sale_layout_cat_id if x.sale_layout_cat_id else ''

        return grouplines(self, ordered_lines, sortkey)
    
    
    def append_price(self, input_price, category_no):
        """
            Append price for category and save it
            @input_price: Price to be saved
            @category_no: Category no
            @return: None - it's a void. We'll save all data in member variable
        """            
#         if category_no in self.data_dict:
#             value = self.data_dict[category_no]
#             value += input_price
#             self.data_dict[category_no] = value
#         else:
#             self.data_dict[category_no] = input_price
        
        return None
    
    
    def get_gross_price(self, line, language, currency_id):
        """
            Get gross price for given order position together with currency
            @object: Order position
            @language: Actual language
            @currency_id: Currency id
            @return: Gross price as string together with currency 
        """
        return self.env["eq_report_helper"].get_gross_price(line, language, currency_id)
    
    def get_gross_price_as_float(self, line, language, currency_id):
        """
            Get gross price for given order position as float
            @object:
            @language:
            @currency_id:
            @return: Gross price as float
        """
        return self.env["eq_report_helper"].get_gross_price_as_float(line, language, currency_id)
    
    def calculate(self, input, category_no):
        """
            Calculate total price without optional products
            @input: Subtotal price - total inkl. optional products
            @category_no: Category no
            @return: Total price without optional products
        """     
                   
#         if category_no in self.data_dict:               # check if we find total price for category
#             if self.display_gross_price is False:
#                 total_price = self.data_dict[category_no]
#                 result = input - total_price
#                 return result
#             else:
#                 total_price = self.data_dict[category_no]            
#                 result = input - total_price
#                 return result
        
        return input
    
    def calculate_sum(self, input, category_no, lines):
        """
            Calculate sum - just a simple total price
            @input: input
            @category_no: category
            @lines: order lines
        """
        result = 0
        
        for line in lines:
            if line.eq_optional is False:
                quantity = line.product_uom_qty
                price = line.price_unit
                result += quantity * price  

        return result
    
    def get_standard_price(self, object, language, currency_id):
        return self.env["eq_report_helper"].get_standard_price(object, language, currency_id)
    
    def get_tax(self, object, tax_id, language, currency_id):
        amount_net = 0
        for line in object.order_line:
            if tax_id.id in [x.id for x in line.tax_id] and not line.eq_optional:
                if tax_id.price_include:                
                    amount_net += line.price_total
                else:
                    amount_net += line.price_subtotal
                 
        tax_amount = tax_id.compute_all(amount_net, line.order_id.currency_id, 1.0, product=line.product_id, partner=line.order_id.partner_shipping_id)
        amount = 0
        for amount_data in tax_amount.get('taxes',False): 
            if amount_data.get('amount',False) > 0:
                amount += amount_data['amount']
            
        return self.env["eq_report_helper"].get_price(amount, language, 'Sale Price Report', currency_id)
    
    def has_tax_amount(self, object, tax_id):
        amount_net = 0
        for line in object.order_line:
            if tax_id.id in [x.id for x in line.tax_id] and not line.eq_optional:                
                amount_net += line.price_subtotal
                 
        tax_amount = tax_id.compute_all(amount_net, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)

        for amount in tax_amount.get('taxes',False): 
            if amount.get('amount',False) > 0:
                return True
        
        return False
    
    
    
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    
    @api.depends('discount', 'price_unit', 'product_uom_qty', 'price_subtotal')
    def _compute_discount(self):
        """
        Berechnet den Rabattwert für eine Position
        :return:
        """

        # NOTE: Deactivated on 09.01.2019 because of Ticket 5280
        """        
        for record in self:
            # Ticket 4142: neue Berechnung für Rabatt: Preis * Menge - Betrag inkl. Rabatt
            if record.price_subtotal:
                record.discount_value = record.price_unit * record.product_uom_qty - record.price_subtotal
            else:
                # Falls Betrag noch nicht berchnet wurde, alte Logik nutzen
                # cur_obj = self.pool.get('res.currency')
                # full_price = record.price_unit * record.product_uom_qty
                # subtotal = cur_obj.round(cr, uid, cur, full_price * (1 - (order.eq_percent_discount / 100)))
                # record.discount_value = full_price - subtotal


                record.discount_value = record.discount / 100 * record.price_unit * record.product_uom_qty
        """
        for record in self:
            record.discount_value = record.discount / 100 * record.price_unit * record.product_uom_qty
    
    sale_layout_cat_id = fields.Many2one('sale_layout.category',string='Section')
    get_delivery_date = fields.Char('Delivery')
    discount_value = fields.Float(compute='_compute_discount', string='Discount value', store=False, readonly=True)
    eq_optional = fields.Boolean(string="Optional")
    
    
    def get_qty(self, uom, language):
        return self.env["eq_report_helper"].get_qty(uom, language, 'Sale Quantity Report')
    
    def get_price(self, price, language, currency_id):                
        return self.env["eq_report_helper"].get_price(price, language, 'Sale Price Report', currency_id)
    
    def get_standard_price(self, price, language, currency_id):
        return self.env["eq_report_helper"].get_standard_price(price, language, currency_id)
    