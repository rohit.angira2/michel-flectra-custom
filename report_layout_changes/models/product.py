# -*- coding: utf-8 -*-

from flectra import models, fields, api,_
from datetime import datetime

class Product(models.Model):
    _inherit = 'product.product'
    
    eq_drawing_number = fields.Char('Drawing Number')
    
class ProductTemlpate(models.Model):
    _inherit = 'product.template'
    
    eq_drawing_number = fields.Char('Drawing Number')