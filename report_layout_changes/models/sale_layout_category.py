# -*- coding: utf-8 -*-

from flectra import models, fields, api,_

class SaleLayoutCategory(models.Model):
    _name = 'sale_layout.category'
    _order = 'sequence'
    
    name = fields.Char('Name')
    sequence = fields.Char('Sequence')
    subtotal = fields.Boolean('Add subtotal')
    separator = fields.Boolean('Add separator')
    pagebreak = fields.Boolean('Add pagebreak')