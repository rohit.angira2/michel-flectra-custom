# -*- coding: utf-8 -*-

from flectra import models, fields, api,_
from datetime import datetime
from itertools import groupby

def grouplines(self, ordered_lines, sortkey):
    """Return lines from a specified invoice or sale order grouped by category"""
    grouped_lines = []
    for key, valuesiter in groupby(ordered_lines, sortkey):
        group = {}
        group['category'] = key
        group['lines'] = list(v for v in valuesiter)
        grouped_lines.append(group)

    return grouped_lines

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    eq_ref_number = fields.Char('Sale Order Referenc')
    min_date = fields.Datetime('Min Date')
    
    
    def sale_layout_lines(self, order_id=None):
        """
        Returns order lines from a specified sale ordered by
        sale_layout_category sequence. Used in sale_layout module.

        :Parameters:
            -'order_id' (int): specify the concerned sale order.
        """
        ordered_lines = self.browse(order_id).move_lines

        ordered_lines = sorted(ordered_lines, key=lambda x: x.eq_pos_no, reverse=False)

        sortkey = lambda x: x.sale_line_id.sale_layout_cat_id if x.sale_line_id and x.sale_line_id.sale_layout_cat_id else ''
        
        return grouplines(self, ordered_lines, sortkey)
    
    
    def get_weight(self, object, language):
        return self.env["eq_report_helper"].get_qty(object, language, 'Sale Weight Report')
    
    def get_qty(self, object, language):
        return self.env["eq_report_helper"].get_qty(object, language, 'Sale Quantity Report')
    
    
class StockMove(models.Model):
    _inherit = 'stock.move'
    
    eq_pos_no = fields.Integer('Seq') 
    
    
    