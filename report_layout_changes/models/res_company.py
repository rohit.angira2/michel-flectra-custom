# -*- coding: utf-8 -*-

from flectra import models, fields, api,_

class ResCompany(models.Model):
    _inherit = 'res.company'

    eq_custom_1 = fields.Char('Chief labeling', help="The content of this field may be used in the header or footer of reports.")
    eq_custom_2 = fields.Char('1st person', help="The content of this field may be used in the header or footer of reports.")
    eq_custom_3 = fields.Char('2nd person', help="The content of this field may be used in the header or footer of reports.")
    eq_custom_4 = fields.Char('3rd person', help="The content of this field may be used in the header or footer of reports.")
    eq_house_no = fields.Char('House number')