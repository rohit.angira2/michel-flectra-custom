# -*- coding: utf-8 -*-
##############################################################################
#                 @author IT Admin
#
##############################################################################

{
    'name': 'Report Layout Changes',
    'version': '11.0',
    'description': ''' Changes in Report Layout
    ''',
    'author': 'Vivek Shukla',
    'website': 'www.simbeez.com',
    'depends': [
        'sale'
    ],
    'data': [
#              'data/ir_sequence_data.xml',
            'data/inactive_views_data.xml',
             
            'views/eq_css.xml', 
            'views/paper_formate.xml',
            'views/footer.xml',
            'views/category_template.xml',
            'views/extra_templates.xml',
            
            'views/sale_order_report.xml',
            'views/invoice_pro_forma_report.xml',
            'views/stock_picking_report.xml',
            'views/stock_picking_return_report.xml',

            
            'views/res_partner.xml',
            'views/sale_order.xml',
            'views/res_company.xml',
        ],
    'application': False,
    'installable': True,
    'currency': 'USD',
    'license': 'AGPL-3',
}
