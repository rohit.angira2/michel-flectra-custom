# -*- coding: utf-8 -*-
# Part of flectra. See LICENSE file for full copyright and licensing details.

{
    'name': 'Website Sales Autoflow',
    'category': 'Website',
    'version': '0.1',
    "depends": ["website_sale",'sale'],
    'description': """
    """,
    'data': [
             ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
