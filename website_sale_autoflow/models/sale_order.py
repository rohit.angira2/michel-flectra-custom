# -*- coding: utf-8 -*-

from flectra import models, api
from flectra.exceptions import ValidationError, UserError

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.multi
    def action_confirm(self):
        if self.website_id:
            self.with_context(send_email=True)._action_confirm()
            self.action_done()
        else:
            super(SaleOrder, self).action_confirm()
        return True
    
    def action_mark_as_paid(self):
        """ Mark directly a sales order as paid if:
                - State: Quotation Sent, or sales order
                - Provider: wire transfer or manual config
            The transaction is marked as done
            The invoice may be generated and marked as paid if configured in the website settings
            """
        self.ensure_one()
        if self.can_directly_mark_as_paid:
            self.with_context(send_email=True).action_confirm()
            if self.env['ir.config_parameter'].sudo().get_param('website_sale.automatic_invoice', default=False):
                self.payment_tx_id._generate_and_pay_invoice()
            self.payment_tx_id.state = 'done'
        else:
            raise ValidationError(_("The quote should be sent and the payment acquirer type should be manual or wire transfer"))