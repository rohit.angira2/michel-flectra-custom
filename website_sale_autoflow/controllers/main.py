# -*- coding: utf-8 -*-

from flectra import http
from flectra.http import request
from flectra.tools import float_is_zero
from flectra.addons.website_sale.controllers.main import WebsiteSale


class WebsiteSaleCustom(WebsiteSale):
    
    def get_outstanding_info(self, order):
        company_id = order.company_id.id
        p = order.partner_id if not company_id else order.partner_id.with_context(force_company=company_id)
        if p.parent_id:
            rec_account = p.parent_id.property_account_receivable_id
        else:
            rec_account = p.property_account_receivable_id
        account_id = rec_account.id
        domain = [('account_id', '=', account_id),
                ('partner_id', '=', request.env['res.partner']._find_accounting_partner(order.partner_id).id),
                ('reconciled', '=', False),
                '|',
                '&', ('amount_residual_currency', '!=', 0.0), ('currency_id','!=', None),
                '&', ('amount_residual_currency', '=', 0.0), '&', ('currency_id','=', None), ('amount_residual', '!=', 0.0)]
        domain.extend([('credit', '>', 0), ('debit', '=', 0)])
        lines = request.env['account.move.line'].sudo().search(domain)
        currency_id = order.currency_id
        amount_to_show = 0
        if len(lines) != 0:
            for line in lines:
                # get the outstanding residual value in invoice currency
                if line.currency_id and line.currency_id == currency_id:
                    amount_to_show = abs(line.amount_residual_currency)
                else:
                    amount_to_show = line.company_id.currency_id.with_context(date=line.date).compute(abs(line.amount_residual), order.currency_id)
                if float_is_zero(amount_to_show, precision_rounding=currency_id.rounding):
                    continue
        if amount_to_show > order.amount_total:
            order.action_confirm()

    @http.route(['/shop/confirmation'], type='http', auth="public", website=True)
    def payment_confirmation(self, **post):
        """ End of checkout process controller. Confirmation is basically seing
        the status of a sale.order. State at this point :

         - should not have any context / session info: clean them
         - take a sale.order id, because we request a sale.order and are not
           session dependant anymore
        """
        sale_order_id = request.session.get('sale_last_order_id')
        if sale_order_id:
            order = request.env['sale.order'].sudo().browse(sale_order_id)
            #add by varun - Confirm Sale order if payment transaction is done from wire transfer
            if order.can_directly_mark_as_paid:
                self.get_outstanding_info(order)
            
            return request.render("website_sale.confirmation", {'order': order})
        else:
            return request.redirect('/shop')