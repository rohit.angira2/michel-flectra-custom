# -*- coding: utf-8 -*-

import base64
import requests
import json

class ShipCloudAPI(object):
    """docstring for ."""
    
    def __init__(self, endpoint, token):
        self.endpoint = endpoint
        self.token = token
        
    def post_data(self, url, data):
        print(url)
        response = requests.post(
            url=url,
            data=json.dumps(data),
            auth=(self.token, ''),
            headers =  {'Content-type': 'application/json', 'Accept': 'text/plain'}
        )
        return response
    
    def get_data(self, url, params):
        response = requests.get(
            url=url,
            params=params,
            auth=(self.token, '')
        )
        return response
    
    @staticmethod
    def parse_response(data):
        print json.dumps((data.json()),indent=2)
        return data
    
    @property
    def rates_endpoint(self):
        return self.endpoint + 'shipment_quotes'

    @property
    def shipments_endpoint(self):
        return self.endpoint + 'shipments'

    @property
    def carriers_endpoint(self):
        return self.endpoint + 'carriers'

    @property
    def addresses_endpoint(self):
        return self.endpoint + 'addresses'

    def get_carriers(self):
        return self.parse_response(self.get_data(self.carriers_endpoint, None))

    def create_address(self):
        data = {
          "company": "Muster-Company",
          "first_name": "Ecombucket",
          "last_name": "Mustermann",
          "street": "Musterstraße",
          "street_no": "42",
          "zip_code": "54321",
          "city": "Musterstadt",
          "country": "DE",
          "phone": "555-555"
        }
        return self.parse_response(self.post_data(self.addresses_endpoint, data))

    def create_shipment_quotes(self):
        data= {
          "carrier": "dpd",
          "service": "standard",
          "to": {
            "street": "Holbeinstr",
            "street_no": "49",
            "zip_code": "04229",
            "city": "Leipzig",
            "country": "DE"
          },
          # "from": {
          #   "street": "Holzstr",
          #   "street_no": "5",
          #   "zip_code": "80469",
          #   "city": "Munich",
          #   "country": "DE"
          # },
          "package": {
            "weight": 3.5,
            "length": 20,
            "width": 20,
            "height": 20
          },
           "package": {
             "weight": 4.5,
             "length": 20,
             "width": 20,
             "height": 20
           }
        }
        return self.parse_response(self.post_data(self.rates_endpoint, data))

    def create_shipments(self):
        data = {
            "to": {
                "company": "Receiver Inc.",
                "first_name": "Max",
                "last_name": "Mustermann",


                "street": "Holbeinstr",
                "street_no": "49",
                "zip_code": "04229",
                "city": "Leipzig",
                "country": "DE"
            },
            "package": {
                "weight": 1.5,
                "length": 20,
                "width": 20,
                "height": 20,
                "type": "parcel"
            },
                "carrier": "dpd",
                "service": "standard",
                "reference_number": "ref123456",
                "notification_email": "person@example.com",
                "create_shipping_label": True
        }
        return self.parse_response(self.post_data(self.shipments_endpoint, data))
if __name__ == '__main__':
    # https://developers.shipcloud.io/concepts/#supported-services
    # https://developers.shipcloud.io/examples/#dhl-bulk-shipments
    url = "https://api.shipcloud.io/v1/"
    token = '8fc0d91a03cf3ed1ac01e5acb4ac47e1'
    sdk = ShipCloudAPI(endpoint=url, token=token)
    response = sdk.create_shipment_quotes()
    print(response)

    
    