# -*- coding: utf-8 -*-

from flectra import api, fields, models
from collections import defaultdict
from flectra.exceptions import ValidationError, UserError
from .tools import SCRuleSet, SCRule, sc_convert_weight, sc_convert_dimension

import requests
import json
import logging

_logger = logging.getLogger(__name__)

def get_weight(operation):
    total_weight = operation.product_qty * operation.product_id.weight
    return sc_convert_weight(total_weight)

def get_value(operation):
    picking = operation.picking_id
    picking_currency = picking.sale_id.currency_id or picking.company_id.currency_id
    company_currency = picking.company_id.currency_id
    value = operation.product_qty * operation.product_id.standard_price
    return company_currency.compute(value, picking_currency)

SC_CUSTOMSITEM_RULESET = SCRuleSet(
    SCRule("description", "product_id.name", required=True),
    SCRule("quantity", 'product_qty', convert_fun=lambda _op, value: str(int(value))),
    SCRule("weight", 'self', required=True, convert_fun=lambda operation, _v: get_weight(operation)),
    SCRule("value", 'self', required=True, convert_fun=lambda operation, _v: get_value(operation)),
    SCRule("hs_tariff_number", 'product_id.hs_code'),
    SCRule("origin_country", 'picking_id.company_id.country_id.code'),
)

SC_CUSTOMSINFO_RULESET = SCRuleSet(
    # SCRule("customs_items", "pack_operation_product_ids", required=True,
    #        convert_fun=lambda _p, operations: operations.mapped(lambda o: o.sc_customsitem_create())),
    SCRule("contents_type", "self", required=True, convert_fun=lambda _p, _v: "merchandise"),  # TODO: improve this
    SCRule("contents_explanation", "self", convert_fun=lambda _p, _v: False),  # TODO
    SCRule("restriction_type", "self", required=True, convert_fun=lambda _p, _v: 'none'),
    SCRule("customs_certify", "self", convert_fun=lambda _p, _v: True),
    SCRule("customs_signer", "company_id.shipping_responsible_id.name", required=True),
    SCRule("eel_pfc", "self", convert_fun=lambda _p, _v: "NOEEI 30.37(a)"),  # TODO: improve this
)

def get_package_details(picking, field):
    pack_type = picking.package_ids[:1].packaging_id
    if not pack_type:
        return False
    if pack_type.package_carrier_type.lower() == picking.carrier_id.delivery_type.lower():
        return pack_type.shipper_package_code if field == "predefined_package" else False
    return sc_convert_dimension(pack_type[field]) if field != "predefined_package" else False

SC_PARCEL_RULESET = SCRuleSet(
    SCRule("weight", "shipping_weight", required=True, convert_fun=lambda picking, value: sc_convert_weight(value)),
    SCRule("length", 'package_ids', convert_fun=lambda picking, value: get_package_details(picking, "length")),
    SCRule("width", 'package_ids', convert_fun=lambda picking, value: get_package_details(picking, "width")),
    SCRule("height", 'package_ids', convert_fun=lambda picking, value: get_package_details(picking, "height")),
    SCRule("predefined_package", 'package_ids',
           convert_fun=lambda picking, value: get_package_details(picking, "predefined_package")),
)


DeliveryType = [
    ('none', 'None'),
    ('fixed', 'Fixed'),
    ('base_on_rule', 'Based On Rule')
]
APIUoM = [
    ('LB', 'LB'),
    ('KG', 'KG'),
    ('OZ', 'OZ')
]

BasicAddress = ['name', 'street', 'street2', 'zip', 'city', 'phone', 'email','first_name']

BASE_ENDPOINT = "https://api.shipcloud.io/v1/"

class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'
    
    delivery_type = fields.Selection(selection=DeliveryType,string='Price Computation',default='fixed',required=True)
    uom_id = fields.Many2one('product.uom',string='flectra Product UoM',help='Shipping Equivalent UoM in flectra')
    delivery_uom = fields.Selection(selection=APIUoM,string='API UoM',help='Default UoM in flectra')
    packaging_id = fields.Many2one('product.packaging',string='Packaging')
    default_product_weight = fields.Float(default=1,string='Default Product  Weight',help="Default  weight  will use in  package if product not have weight")
    is_shipcloud = fields.Boolean(string='ShipCloud Carrier')
    shipcloud_account = fields.Char(string='Account Token')
    shipcloud_service = fields.Char(string='Service Code',copy=False)
    
    
    @api.model
    def sc_get_shipping_price_from_so(self, orders):
        if self.free_over:
            total_delivery = 0
            for line in orders.order_line.filtered('is_delivery'):
                tax_amount = 0.0
                taxes = line.tax_id.compute_all(line.price_unit * (1-(line.discount or 0.0)/100.0),quantity=line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)['taxes']
                for t in taxes:
                    tax_amount += t.get('amount', 0.0)
                total_delivery += line.price_subtotal + tax_amount 

            if (orders.amount_total-total_delivery)>=self.amount:
                return 0

        packagings = self.sc_get_order_package(orders)
        endpoint = BASE_ENDPOINT + 'shipment_quotes'
        price = 0
        errors = ""
        sess  = requests.Session()
        sess.auth = (self.shipcloud_account, '')
        sess.headers.update({'Content-type': 'application/json', 'Accept': 'text/plain'})
        delivery_type = orders.delivery_type
        carrier = delivery_type.split('_')[-1]
        to_address = self.sc_get_shipment_recipient_address(order=orders)
        order_line = orders.order_line.filtered(lambda line:line.is_delivery == False)
        shipping_weight = map(lambda line:line.product_uom_qty * (line.product_id.weight or self.default_product_weight),order_line)
        shipping_weight = shipping_weight or self.default_product_weight
        packaging_id = self.packaging_id
        data = {
                  "carrier": carrier,
                  "service": self.shipcloud_service,
                  "to": {
                    "street": to_address.get('street'),
                    "street_no": to_address.get('street_number'),
                    "care_of": to_address.get('care_of') or '',
                    "zip_code": to_address.get('zip'),
                    "city": to_address.get('city'),
                    "country": to_address.get('country_code'),
                    "phone": to_address.get('phone') or '',
                  },
                  "package": {
                    "weight": sum(shipping_weight),
                    "length": packaging_id.length,
                    "width": packaging_id.width,
                    "height": packaging_id.height
                  }
                }
        result = sess.post(
            url=endpoint,
            data=json.dumps(data),
        )
        result_json = result.json()
        if result_json.get('errors'):
            errors += "%r" % result_json.get('errors')
            raise UserError(_(result_json.get('errors')))
        else:
            price = price + (result_json.get('shipment_quote') or {}).get('price') or 0
        return price
    
    
    @api.model
    def sc_send_shipping(self, pickings):
        result = {'exact_price': 0, 'weight': None,
                  'date_delivery': None, 'tracking_number': '', 
                  'attachments': [], 'shipcloud_id':'', 
                  'shipcloud_tracking_url':'','number_of_packages':0}

        to_address = self.sc_get_shipment_recipient_address(picking=pickings)
        endpoint = BASE_ENDPOINT + 'shipments'
        sess  = requests.Session()
        sess.auth = (self.shipcloud_account, '')
        sess.headers.update({'Content-type': 'application/json', 'Accept': 'text/plain'})
        delivery_type = pickings.delivery_type
        carrier = delivery_type.split('_')[-1]
        reference_number = 'Customer Order' + pickings.name
        if pickings.origin: reference_number = 'Customer Order Reference ' + pickings.origin
        weight = 0
        package_ids = pickings.package_ids
        shipping_weight = pickings.package_ids.mapped('shipping_weight')
        for package in package_ids[:1]:
            package_data = package.sudo().read([])[0]
            data = {
                    "to": {
                            "company": to_address.get('company_name') or  "",
                            "first_name": to_address.get('first_name'),
                            "last_name": to_address.get('last_name') or "",
                            "street": to_address.get('street')[:26],
                            "street_no": to_address.get('street_number'),
                            "care_of": to_address.get('care_of') or '',
                            "city": to_address.get('city'),
                            "zip_code": to_address.get('zip'),
                            "country": to_address.get('country_code'),
                            "phone": to_address.get('phone') or '',
                    },
                    "package": {
                            "weight": sum(shipping_weight),
                            "length": package_data.get('length'),
                            "width": package_data.get('width'),
                            "height": package_data.get('height'),
                            "type": "parcel"
                    },
                    "carrier": carrier,
                    "service": self.shipcloud_service,
                    "reference_number": reference_number,
                    "notification_email": to_address.get('email'),
                    "create_shipping_label": True
                }
            response = sess.post(
                url=endpoint,
                data=json.dumps(data),
            )
            result_json = response.json()
            _logger.info("==========%r=========" % (result_json))
            if result_json.get('errors'):
                raise ValidationError(result_json.get('errors'))
            carrier_tracking_no = result_json.get('carrier_tracking_no')
            label_url = result_json.get('label_url')
            tracking_url = result_json.get('tracking_url')
            shipcloud_id = result_json.get('id')
            price = result_json.get('price')

            if carrier_tracking_no and label_url:
                result['attachments'].append(
                                            (delivery_type + ' ' + 
                                             str(carrier_tracking_no) + '.pdf', 
                                             requests.get(label_url).content))
                result['exact_price'] += int(price)
                result['tracking_number'] = result['tracking_number'] + ',' + carrier_tracking_no
                result['shipcloud_id'] = result['shipcloud_id'] + ',' + shipcloud_id
                result['shipcloud_tracking_url'] = result['shipcloud_tracking_url'] + ',' + tracking_url
                weight +=  package_data.get('shipping_weight')

        result['weight'] = weight
        result['weightnumber_of_packages'] = len(package_ids)
        return result
    
    @api.model
    def sc_get_tracking_link(self, pickings):
        return pickings.shipcloud_tracking_url.split(',')[0]
    
    @api.multi
    def get_price_v1(self, field_name, arg=None, context=None):
        order = self.env['sale.order'].browse(self._context.get('order_id', None))
        total_delivery = 0
        for line in order.order_line:
            if line.is_delivery:
                pass
                tax_amount = 0.0
                taxes = line.tax_id.compute_all(line.price_unit * (1-(line.discount or 0.0)/100.0),quantity=line.product_uom_qty, product=line.product_id, partner=self.partner_shipping_id)['taxes']
                for t in taxes:
                    tax_amount += t.get('amount', 0.0)
                total_delivery += line.price_subtotal + tax_amount
        if self.delivery_type in ['fixed', 'base_on_rule']:
            pass
#             res = super(DeliveryCarrier, self).get_price(
#                 field_name, arg, context=self._context)
#             return res
        else:
            result = {}
            price = 0
            available = False
            warning_message = None
            error_message = None

            try:
                price = order.carrier_id.get_shipping_price_from_so(order)
                # if price is not None:
                available = True
            except ValidationError as e:
                error_message = '%s' % (e)
                order.message_post(body=str(e), subject="%s Error:"%(self.name))
                _logger.info(
                '**********1111***************-ERROR RECIVED IN GET_PRICE-****************')
            except Exception as e:
                error_message = '%s' % (e)
                order.message_post(body=str(e), subject="%s Error:"%(self.name))
                _logger.info(
                    '**********222***************-ERROR RECIVED IN GET_PRICE-****************')
            # if price:
            #     price+=self.get_extra_charges(order)


            result[self.id] = {
            'price': price,
            'available': available,
            'warning_message':warning_message,
            'error_message':error_message,
            }
            return result
    
    @api.model
    def sc_get_order_package(self, order):
        result=[]
        if order.create_package=='auto':
            packagings =self.sc_group_by_packaging(order)
            for packaging_id,lines in packagings.items():
                for line in lines:
                    result.extend(self.sc_get_product_package(line,packaging_id))
            result=self.sc_merge_half_package(result)
        else:
            return map(lambda sc_packaging_id:dict(
                    packaging_id = sc_packaging_id.packaging_id.id,
                    weight = sc_packaging_id.weight,
                    width=sc_packaging_id.width,
                    length=sc_packaging_id.length,
                    height=sc_packaging_id.height
                ),order.sc_packaging_ids)
        return result
    
    @api.model
    def sc_group_by_packaging(self, order=None, pickings=None):
        packagings = defaultdict(list)
        if order:
            for line in order.order_line:
                if  line.state == 'cancel':
                    continue
                product_id = line.product_id
                if (not product_id or line.is_delivery):
                    continue
                packaging_id = self.sc_get_packaging_id(product_id=product_id)
                packagings[packaging_id].append(line)

        else:
            for package_id in pickings.package_ids:
                packaging_id = self.sc_get_packaging_id(package_id=package_id)
                packagings[packaging_id].append(package_id)
        return dict(packagings.items())
    
    
    @api.model
    def sc_get_packaging_id(self, product_id=None, package_id=None):
        pck_id=self.packaging_id
        packaging_id=None
        if product_id:
            packaging_ids = product_id.sc_packaging_ids.filtered(lambda pck_id:pck_id.package_carrier_type == self.delivery_type)
            packaging_id = packaging_ids and packaging_ids[0]
        elif package_id:
            packaging_id = package_id.packaging_id
        packaging_id = packaging_id if packaging_id else pck_id
        if packaging_id: return packaging_id 
        raise ValidationError('Packaging is not set of product and carrier %s as well.' % (self.name))
    
    
    @api.model
    def sc_get_product_package(self,line,packaging_id,partial_package=None):
        """Return package count
            major package [package_count,capacity], minor package [package_count=1,capacity]

        """
        dimension=dict(height=packaging_id.height,width=packaging_id.width,length=packaging_id.length,packaging_id=packaging_id.id)
        result=list()
        line_price = 0
        if partial_package:
            price_unit = sum(map(lambda item:item.get('price_unit',0),line))
            if price_unit:
                line_price = price_unit/len(line)
        else:
            line_price = sum(map(lambda item:line.price_unit,line))/len(line)
        product_qty,product_weight = self.get_package_attribute(line,packaging_id,partial_package)
        max_qty, max_weight = int(packaging_id.qty) and int(packaging_id.qty) or 1 ,packaging_id.max_weight  and packaging_id.max_weight or 1
        qty_capacity = int(max_weight//product_weight)
        if qty_capacity and qty_capacity < max_qty:
            package_count = product_qty//qty_capacity
            if package_count:
                multi_pckg_qty_capacity = dimension.copy()
                multi_pckg_qty_capacity.update(dict(
                    weight = product_weight * qty_capacity,
                    full_capacity = True,
                    sc_cover_amount  = line_price * qty_capacity,
                ))
                result += [multi_pckg_qty_capacity] * package_count
            if product_qty % qty_capacity:
                single_pckg_qty_capacity = dimension.copy()
                single_pckg_qty_capacity.update(dict(
                    weight = product_weight * (product_qty % qty_capacity),
                    full_capacity = False,
                    sc_cover_amount = line_price * (product_qty % qty_capacity),
                ))
                result += [single_pckg_qty_capacity] * 1
        else:
            package_count = product_qty//max_qty
            if package_count:
                multi_pckg_max_qty = dimension.copy()
                multi_pckg_max_qty.update(dict(
                    weight = product_weight * max_qty,
                    full_capacity = True,
                    sc_cover_amount = line_price * max_qty,
                    ))
                result += [multi_pckg_max_qty] * package_count
            if product_qty % max_qty:
                single_pckg_max_qty = dimension.copy()
                single_pckg_max_qty.update(dict(
                    weight = product_weight * (product_qty % max_qty),
                    full_capacity = False,
                    sc_cover_amount = line_price * (product_qty % max_qty),
                    ))
                result += [single_pckg_max_qty] * 1
        return result
    
    @api.model
    def get_package_attribute(self,line,packaging_id,partial_package):
        product_qty,product_weight=1,1
        if not partial_package:
            product_id = line.product_id
            product_qty,product_weight = int(line.product_uom_qty) and int(line.product_uom_qty) or 1,product_id.weight and product_id.weight or 1
        else:
            product_weight = sum(map(lambda item:item.get('weight'),line))
        return  product_qty,product_weight
    
    
    @api.model
    def sc_merge_half_package(self, items):
        data = defaultdict(list)
        for item in filter(lambda item:not item.get('full_capacity'),items):
            packaging_id = item.get('packaging_id')
            if type(packaging_id)==int:
                packaging_id =self.env['product.packaging'].browse(packaging_id)
            data[packaging_id].append(item)
        new_dict=dict()
        for key,value  in data.items():
            if len(value)>1:
                new_dict[key]=value
                for val in value:
                    items.remove(val)
        for packaging_id,lines in new_dict.items():
            items.extend(self.sc_get_product_package(lines,packaging_id,partial_package=True))
        return items
    
    
    def sc_get_shipment_recipient_address(self, order=None, picking=None):
        if order:
            recipient = order.partner_shipping_id if order.partner_shipping_id else order.partner_id
        else:
            recipient = picking.partner_id
        if not len(recipient):
            raise ValidationError('Please check partner address.')
        return self.sc_get_shipment_address(recipient)
    
    def sc_get_shipment_address(self, entity):
        data  = entity.sudo().read(BasicAddress)[0]
        company_name = None
        if entity.parent_id:
            company_name = entity.parent_id.name

        data['country_name'] = entity.country_id.name
        data['country_code'] = entity.country_id.code
        data['state_name'] = entity.state_id.name
        data['state_code'] = entity.state_id.code
        data['company_name'] = company_name
#         data['street_number'] = entity.street.split(" ")[-1]
#         street_name = ""
#         for s in entity.street.split(" ")[:-1]:
#             street_name = street_name + " " + s
        data['street'] = entity.street_name
        data['street_number'] = entity.street_number
        if entity.is_packstation_address:
            data['care_of'] = entity.street2
            
        data['last_name'] = entity.last_name
        keys = ['name','street','first_name']
        for key  in keys:
            if not data.get(key):
                raise ValidationError('No data set in %s'%(key))
        return data
    
    def get_shipping_price_from_so(self, orders):
        delivery_type = self.delivery_type
        _logger.info("=++%r==========" % hasattr(self, '%s_get_shipping_price_from_so' % delivery_type))
        if hasattr(self, '%s_get_shipping_price_from_so' % delivery_type):
            return getattr(self, '%s_get_shipping_price_from_so' % self.delivery_type)(orders)
    
    def send_shipping(self, pickings):
        self.ensure_one()
        if hasattr(self, '%s_send_shipping' % self.delivery_type):
            return getattr(self, '%s_send_shipping' % self.delivery_type)(pickings)
    
    def get_tracking_link(self, pickings):
        self.ensure_one()
        if hasattr(self, '%s_get_tracking_link' % self.delivery_type):
            return getattr(self, '%s_get_tracking_link' % self.delivery_type)(pickings)
        
    def _get_default_uom(self):
        uom_categ_id = self.env['ir.model.data'].xmlid_to_res_id('product.product_uom_categ_kgm')
        weight_uom_id = self.env['product.uom'].search([('category_id', '=', uom_categ_id),('factor', '=', 1)])[0]
        return weight_uom_id
    
    def sc_get_shipment_shipper_address(self, order=None, picking=None):
        if order:
            shipper = order.warehouse_id.partner_id
        else:
            shipper = picking.picking_type_id.warehouse_id.partner_id
        if not len(shipper):
            raise ValidationError('Please check warehouse address.')
        return self.sc_get_shipment_address(shipper)
    
    def sc_convert_currency(self, amount, to_currency_code):
        order_currency = self.currency_id or self.company_id.currency_id
        to_currency = self.env['res.currency'].search([('name', '=ilike', to_currency_code)])
        if to_currency and to_currency != order_currency:
            return order_currency.compute(amount, to_currency)
        return amount
    
    @api.model
    def get_shipping_price_for_order(self, orders):
        for order in orders:
            if order.carrier_id.delivery_type not in ['fixed','base_on_rule']:
                if order.create_package=='manual' and len(order.sc_packaging_ids)==0:
                    raise ValidationError(_('Create the package first for manual packaging of order %s.'%(order.name)))
        context = dict(self._context)
        try:
            result = self.with_context(context).get_shipping_price_from_so(orders)
            order.delivery_rating_success = True
            order.delivery_price = result
            order.delivery_message = None
        except ValidationError as e:
            error_message = '%s' % (e.value)
            order.message_post(body=error_message, subject="%s Error:"%(self.name))
            _logger.info(
            '*************************-ERROR RECIVED IN GET_PRICE-****************')
            order.delivery_rating_success = False
            order.delivery_price = 0.0
            order.delivery_message = error_message
        except Exception as e:
            error_message = '%s' % (e)
            order.message_post(body=str(e), subject="%s Error:"%(self.name))
            _logger.info(
            '*************************-ERROR RECIVED IN GET_PRICE-****************')
            order.delivery_rating_success = False
            order.delivery_price = 0.0
            order.delivery_message = error_message
    
    
    @api.model
    def sc_validate_data(self, order=None, pickings=None):
        if pickings:
            if not pickings.package_ids:
                raise ValidationError('Create the package before sending to shipper.')
            else:
                package_ids = pickings.package_ids.filtered(lambda package_id: not package_id.packaging_id)
                if len(package_ids):
                    raise ValidationError('Packaging is not set for package %s.' % (','.join(package_ids.mapped('name'))))
    
    
    
    
    
    
    
    
    
    
    