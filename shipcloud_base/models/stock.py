# -*- coding: utf-8 -*-
from flectra import api, fields, models, _
import logging
from flectra.exceptions import ValidationError
_logger = logging.getLogger(__name__)

class StockQuantPackage(models.Model):
    _inherit = "stock.quant.package"
    
    height = fields.Float(related='packaging_id.height')
    width = fields.Float(related='packaging_id.width')
    length = fields.Float(related='packaging_id.length')
    cover_amount = fields.Integer(string='Cover Amount',help='This is the declared value/cover amount for an individual package.')
    description = fields.Text(string='Description',help='The text describing the package.')
    
    @api.multi
    def sc_write(self):
        if self.packaging_id.package_carrier_type not in ['none']:
            if self.shipping_weight<0:
                msz=_('Shipment weight must be positive (shipping_weight>0).')
                raise ValidationError(msz)
            elif self.packaging_id and (self.packaging_id.max_weight < self.shipping_weight):
                msz = _('Shipment weight should be less then {max_weight} kg  as {max_weight} kg is the max weight limit set  for {name}  .'.format(max_weight=self.packaging_id.max_weight,name=self.packaging_id.name))
                _logger.info("Weight Check: %s  ",msz)
                raise ValidationError(msz)
            
            
class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    @api.multi
    def manage_package_type(self):
        self.ensure_one()
        delivery_type = self.picking_id.carrier_id.delivery_type not in ['base_on_rule', 'fixed']  and self.picking_id.carrier_id.delivery_type or 'none'
        self.picking_id.sc_update_package(self.result_package_id)
        return {
            'name': _('Package Details'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'stock.quant.package',
            'view_id': self.env.ref('shipcloud_base.stock_quant_package').id,
            'target': 'new',
            'res_id': self.result_package_id.id,
            'context': {
                'current_package_carrier_type': delivery_type ,
                'no_description':not(delivery_type in ['fedex','dhl','ups','auspost'] and delivery_type or False ),
                'no_cover_amount' : not(delivery_type in ['fedex','dhl','ups','auspost'] and delivery_type or False ),
            },
        }
