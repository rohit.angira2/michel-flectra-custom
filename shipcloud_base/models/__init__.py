# -*- coding: utf-8 -*-
# Part of flectra. See LICENSE file for full copyright and licensing details.

from . import carrier
from . import order
from . import package
from . import picking
from . import stock
from . import res_partner