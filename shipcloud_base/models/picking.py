# -*- coding: utf-8 -*-

from flectra import api, fields, models, _
from flectra.exceptions import ValidationError, UserError
from flectra.tools.float_utils import float_compare, float_is_zero, float_round

import logging
_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    @api.multi
    def auto_put_in_pack(self):
        QuantPackage = self.env["stock.quant.package"]
        package = False
        for pick in self:
            if not pick.move_line_ids:
                pass
#                 pick.do_prepare_partial()
            qty_done_zero = [x for x in pick.move_line_ids if x.qty_done < x.product_qty and (not x.result_package_id)]
#             map(lambda i:i.write(dict(qty_done=( i.product_qty-i.qty_done))),qty_done_zero)
            for line in qty_done_zero:
                line.write({'qty_done':line.product_qty - line.qty_done})
            operations = [x for x in pick.move_line_ids if x.qty_done ==x.product_qty and (not x.result_package_id)]
            carrier_id =  pick.carrier_id
            default_product_weight = carrier_id.default_product_weight
            for operation in operations:
                product_id = operation.product_id
                packaging_id =  carrier_id.sc_get_packaging_id(product_id=product_id)
                qty_done = operation.qty_done
                vals =dict(
                    shipping_weight=qty_done * (product_id.weight or default_product_weight),
                    packaging_id= packaging_id.id,
                    cover_amount=packaging_id.get_cover_amount(product_id.list_price*qty_done),
                    height=packaging_id.height,
                    width=packaging_id.width,
                    length=packaging_id.length,
                )
                package = QuantPackage.create(vals)
                operation.write({'result_package_id': package.id})
                
    @api.model
    def get_picking_price(self,package_id):
        move_line_ids = self.env['stock.move.line'].search(
            [('result_package_id', '=', package_id.id)])
        return  sum([x.qty_done * x.product_id.list_price for x in move_line_ids])
    
    
    @api.model
    def sc_update_package(self,package_id=None):
        if self.carrier_id.delivery_type not in ['base_on_rule', 'fixed']:
            packaging_id = package_id.packaging_id
            if package_id and (not packaging_id):
                packaging_id = self.carrier_id.packaging_id
                package_id.packaging_id = packaging_id.id
            amount =self.get_picking_price(package_id)
            package_id.cover_amount = packaging_id.get_cover_amount(amount)
        return True
    
#     @api.multi
#     def put_in_pack(self):
#         self.ensure_one()
#         carrier_id=self.carrier_id
#         delivery_type = carrier_id.delivery_type not in ['base_on_rule', 'fixed']  and carrier_id.delivery_type or 'none'
#         if not self.env['product.packaging'].search_count([('package_carrier_type', '=', delivery_type)]):
#             return False

#         if package_id:
#             package_id = self.env["stock.quant.package"].browse(package_id)
#             self.sc_update_package(package_id)
#             move_line_ids = self.env['stock.move.line'].search([('result_package_id', '=', package_id.id)])
#             default_product_weight =carrier_id.default_product_weight
#             package_weight = sum([x.qty_done * (x.product_id.weight or default_product_weight) for x in move_line_ids])
#             package_id.shipping_weight = package_weight
#             return {
#                 'name': _('Package Details'),
#                 'type': 'ir.actions.act_window',
#                 'view_mode': 'form',
#                 'res_model': 'stock.quant.package',
#                 'view_id': self.env.ref('shipcloud_base.stock_quant_package').id,
#                 'target': 'new',
#                 'res_id': package_id.id,
#                 'context': {
#                     'current_package_carrier_type': delivery_type,
#                     'no_description':not(delivery_type in ['fedex','dhl','ups','auspost'] and delivery_type or False ),
#                     'no_cover_amount' : not(delivery_type in ['fedex','dhl','ups','auspost'] and delivery_type or False ),
#                 },
#             }
#         else:
#             return package_id


    def _default_uom(self):
        uom_categ_id = self.env.ref('product.product_uom_categ_kgm').id
        return self.env['product.uom'].search([('category_id', '=', uom_categ_id), ('factor', '=', 1)], limit=1)
    
    @api.one
    @api.depends('package_ids')
    def _compute_shipping_weight(self):
        self.shipping_weight =  sum(self.package_ids.mapped('shipping_weight'))
        
    @api.one
    @api.depends('package_ids')
    def _compute_cover_amount(self):
        self.cover_amount = sum(self.package_ids.mapped('cover_amount'))
        
        
    @api.one
    @api.depends('move_line_ids')
    def _compute_packages(self):
        self.ensure_one()
        packs = set()
        for packop in self.move_line_ids:
            if packop.result_package_id:
                packs.add(packop.result_package_id.id)
            elif packop.package_id and not packop.product_id:
                packs.add(packop.package_id.id)
        self.package_ids = list(packs)
        
        
    
    package_ids = fields.Many2many(
        'stock.quant.package',
        compute='_compute_packages',
        string='Packages'
    )
    pack_operation_product_ids=fields.One2many(
        'stock.move.line',
        'picking_id',
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        domain=[('product_id', '!=', False)],
        string='Non pack'
    )
    pack_operation_pack_ids= fields.One2many(
        'stock.move.line',
        'picking_id',
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        domain=[('product_id', '=', False)],
        string='Pack')
    carrier_price = fields.Float(string="Shipping Cost",readonly=True,copy=False)
    delivery_type = fields.Selection(related='carrier_id.delivery_type',readonly=True,copy=False)
    label_genrated = fields.Boolean(string='Label Generated',copy=False)
    shipment_uom_id = fields.Many2one(related='carrier_id.uom_id',readonly="1",help="Unit of measurement for use by Delivery method",copy=False)
    date_delivery = fields.Date(string='Expected Date Of Delivery',help='Expected Date Of Delivery :The delivery time stamp provided by Shipment Service',copy=False,readonly=1)
    weight_shipment = fields.Float(string='Shipment Weight',copy=False,readonly=1)
    shipping_weight = fields.Float(string='Weight for Shipping',compute='_compute_shipping_weight')
    cover_amount = fields.Float(string='Shipment Cover Amount',compute='_compute_cover_amount',copy=False,readonly=1)
    shipcloud_id = fields.Char()
    shipcloud_tracking_url = fields.Char()
    
    
    @api.multi
    def action_cancel(self):
        for record in self:
            if record.label_genrated == True:
                raise ValidationError('Please cancel the shipment before canceling  picking! ')
        return super(StockPicking, self).action_cancel()
    
    @api.multi
    def action_generate_label(self):
        try:
            res = self.carrier_id.send_shipping(self)
            self.carrier_price = res['exact_price']
            self.carrier_tracking_ref = res['tracking_number'][1:]
            self.shipcloud_id = res['shipcloud_id'][1:]
            self.shipcloud_tracking_url = res['shipcloud_tracking_url'][1:]
            self.label_genrated=True
            self.date_delivery=res['date_delivery']
            self.weight_shipment=float(res['weight'])
            
            msg = _("Shipment sent to carrier %s for expedition with tracking number %s") % (self.carrier_id.delivery_type, self.carrier_tracking_ref)
            self.message_post(
                body=msg,
                subject="Attachments of tracking",
                attachments=res['attachments']
            )
        except Exception as e:
            raise ValidationError(e)
    
    @api.multi
    def open_website_url(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_url',
            'name': "Shipment Tracking Page",
            'target': 'new',
            'url': self.carrier_id.get_tracking_link(self)
         }
        
    @api.model
    def unset_fields_prev(self):
        self.carrier_tracking_ref = ''
        self.carrier_price = 0
        self.label_genrated = False
        self.date_delivery = False
        self.weight_shipment = ''
        self.number_of_packages = False
        self.shipcloud_id = False
        return True   
    
    @api.multi
    def button_validate(self):
        self.ensure_one()
        if not self.move_lines and not self.move_line_ids:
            raise UserError(_('Please add some lines to move'))

        # If no lots when needed, raise error
        picking_type = self.picking_type_id
        precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        no_quantities_done = all(float_is_zero(move_line.qty_done, precision_digits=precision_digits) for move_line in self.move_line_ids.filtered(lambda m: m.state not in ('done', 'cancel')))
        no_reserved_quantities = all(float_is_zero(move_line.product_qty, precision_rounding=move_line.product_uom_id.rounding) for move_line in self.move_line_ids)
        if no_reserved_quantities and no_quantities_done:
            raise UserError(_('You cannot validate a transfer if you have not processed any quantity. You should rather cancel the transfer.'))

        if picking_type.use_create_lots or picking_type.use_existing_lots:
            lines_to_check = self.move_line_ids
            if not no_quantities_done:
                lines_to_check = lines_to_check.filtered(
                    lambda line: float_compare(line.qty_done, 0,
                                               precision_rounding=line.product_uom_id.rounding)
                )

            for line in lines_to_check:
                product = line.product_id
                if product and product.tracking != 'none':
                    if not line.lot_name and not line.lot_id:
                        raise UserError(_('You need to supply a lot/serial number for %s.') % product.display_name)
                    elif line.qty_done == 0:
                        raise UserError(_('You cannot validate a transfer if you have not processed any quantity for %s.') % product.display_name)
        
        #Add by varun
        #To do auto put in pack when click on validate button
        if self.delivery_type and self.delivery_type not in ['none','fixed','base_on_rule']:
            self.auto_put_in_pack()
        
        if no_quantities_done:
            view = self.env.ref('stock.view_immediate_transfer')
            wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, self.id)]})
            return {
                'name': _('Immediate Transfer?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.immediate.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        if self._get_overprocessed_stock_moves() and not self._context.get('skip_overprocessed_check'):
            view = self.env.ref('stock.view_overprocessed_transfer')
            wiz = self.env['stock.overprocessed.transfer'].create({'picking_id': self.id})
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.overprocessed.transfer',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

        # Check backorder should check for other barcodes
        if self._check_backorder():
            return self.action_generate_backorder_wizard()
        self.action_done()
        return True
    
    @api.multi
    def send_to_shipper(self):
        self.ensure_one()
        if self.delivery_type not in ['fixed','base_on_rule','none']:
            res = self.carrier_id.send_shipping(self)
            self.carrier_price = res['exact_price']
            self.carrier_tracking_ref = res['tracking_number'][1:]
            self.shipcloud_id = res['shipcloud_id'][1:]
            self.shipcloud_tracking_url = res['shipcloud_tracking_url'][1:]
            self.label_genrated=True
            self.date_delivery=res['date_delivery']
            self.weight_shipment=float(res['weight'])
            
            msg = _("Shipment sent to carrier %s for expedition with tracking number %s") % (self.carrier_id.delivery_type, self.carrier_tracking_ref)
            self.message_post(
                body=msg,
                subject="Attachments of tracking",
                attachments=res['attachments']
            )
        else:
            super(StockPicking, self).send_to_shipper()
                