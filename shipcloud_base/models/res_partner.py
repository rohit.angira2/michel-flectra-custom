# -*- coding: utf-8 -*-

from flectra import models, fields, api

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    first_name = fields.Char('First Name')
    last_name = fields.Char('Last Name')
    is_packstation_address = fields.Boolean('Is Packstation Address')
       
    
    @api.onchange('first_name','last_name')
    def onchange_name(self):
        for record in self:
            if not record.first_name and not record.last_name:
                record.name = ""
            elif record.first_name and not record.last_name:
                record.name = record.first_name
            else:
                record.name = record.first_name + " " + record.last_name
