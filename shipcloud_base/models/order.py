# -*- coding: utf-8 -*-

from flectra import api, fields, models, _, SUPERUSER_ID
import logging
from flectra.exceptions import UserError

_logger = logging.getLogger(__name__)

DeliveryType = [
    ('none', 'None'),
    ('fixed', 'Fixed'),
    ('base_on_rule', 'Rule')
]

class ProductTemplate(models.Model):
    _inherit = "product.template"
    
    sc_packaging_ids = fields.Many2many(
        'product.packaging',
        'product_tmp_product_packaging_rel',
        'product_tmpl_id',
        'packaging_id',
        string='Packaging'
    )
    
class SaleOrder(models.Model):
    _inherit = 'sale.order'
    

    delivery_type = fields.Selection(
        related='carrier_id.delivery_type',
        readonly=True,
        copy=False
    )
    carrier_price = fields.Float(
        string='Actual Carrier Charges',
        help="Actual Carrier Charges receive from api "
    )
    create_package = fields.Selection(
        string='Create Package',
        selection=[('auto','Automatic'),('manual','Manual')],
        default='auto',
        help='Create  automatic package as per packing max weight limit and  max qty',
    )
    sc_packaging_ids = fields.One2many(
        'product.package',
        'order_id',
        string='Product Package'
    )
    
    @api.onchange('carrier_id')
    def onchange_carrier_id(self):
        if self.state in ('draft', 'sent'):
            self.delivery_price = 0.0
            self.delivery_rating_success = False
            self.delivery_message = False
            
    @api.model
    def sc_get_order_package(self):
        return map(lambda sc_packaging_id:dict(
            packaging_id = sc_packaging_id.packaging_id.id,
            weight = sc_packaging_id.weight,
            width=sc_packaging_id.width,
            length=sc_packaging_id.length,
            height=sc_packaging_id.height,
            cover_amount=sc_packaging_id.cover_amount,
            qty=sc_packaging_id.qty,
        ),self.sc_packaging_ids)
    
    
        
    @api.multi
    def get_shipping_price(self):
        simple_order = self.filtered(lambda order:order.carrier_id and order.carrier_id.delivery_type  in ['fixed', 'base_on_rule'])
        orders = self-simple_order
        price_unit = final_price = 0
        if len(orders):
            ctx = dict(self._context)
            ctx['sc_compute_price'] = 1
            for order in orders:
                carrier = order.carrier_id
                if carrier:
                    if order.state not in ('draft', 'sent'):
                        raise UserError(_('The order state have to be draft to add delivery lines.'))
                    if carrier.delivery_type not in ['fixed', 'base_on_rule']:
                        price_unit = order.carrier_id.with_context(ctx).get_shipping_price_for_order(order)
                        # order.delivery_price = price_unit

        for order in simple_order:
            carrier = order.carrier_id
            assert (carrier!=None),_('No carrier set for this order.')
            assert (order.state in ('draft', 'sent')),_('The order state have to be draft to add delivery lines.')
            delivery_carrier=carrier.rate_shipment(order)
            if  delivery_carrier['success']:
                order.delivery_price = delivery_carrier['price']
                order.delivery_rating_success = True
                order.delivery_message = delivery_carrier.get('warning_message') or ''
            else:
                order.delivery_rating_success = True
                order.delivery_message = delivery_carrier.get('warning_message') or ''
                order.delivery_price = None

    
    
    @api.multi
    def delivery_set(self):
        SaleOrderLine = self.env['sale.order.line']
        acc_fp_obj = self.env['account.fiscal.position']
        currency_obj =self.env['res.currency']
        context = dict(self._context)
        self._remove_delivery_line()
        for order in self:
                carrier = order.carrier_id
                assert (carrier!=None),_('No carrier set for this order.')
                assert (order.state in ('draft', 'sent')),_('The order state have to be draft to add delivery lines.')
                context.update({'order_id':order.id,
                'sc_compute_price':1
                })
                
                delivery_carrier=carrier.rate_shipment(order)

                if delivery_carrier['success']:
                    delivery_price = delivery_carrier['price']
                    order.write(dict(delivery_price=delivery_price))
                    fpos = order.fiscal_position_id or False
#                     taxes = carrier.product_id.taxes_id
                    #taxes_ids = fpos.fiscal_position.sudo().map_tax(taxes)
                    # Apply fiscal position
                    taxes = carrier.product_id.taxes_id.filtered(lambda t: t.company_id.id == self.company_id.id)
                    taxes_ids = taxes.ids
                    if self.partner_id and self.fiscal_position_id:
                        taxes_ids = self.fiscal_position_id.map_tax(taxes, carrier.product_id, self.partner_id).ids
 
#                     taxes_ids = acc_fp_obj.map_tax(taxes)
                    SaleOrderLine.create({
                        'order_id': order.id,
                        'name': carrier.name,
                        'product_uom_qty': 1,
                        'product_uom': carrier.product_id.uom_id.id,
                        'product_id': carrier.product_id.id,
                        'price_unit': delivery_price,
                        'tax_id': [(6, 0, taxes_ids)],
                        'is_delivery': True
                    })
                    order.message_post(body='The Shipping Service  Cost  of {price} is successfully added to the Sale order.'.format(price=delivery_price),subject=carrier.delivery_type.upper()+' Shipping Service')
                else:
                    order.carrier_id=None
    
    
    def _get_ddddelivery_methods(self, order):
        carrier_obj = self.env['delivery.carrier']
        context = dict(self._context or dict())
        context.update(dict(order_id=order.id))
        delivery_ids = carrier_obj.search([('website_published','=',True)])
        # Following loop is done to avoid displaying delivery methods who are not available for this order
        # This can surely be done in a more efficient way, but at the moment, it mimics the way it's
        # done in delivery_set method of sale.py, from delivery module
        for delivery_id in carrier_obj.browse(delivery_ids):
            # if delivery_id.delivery_type in ['fixed','base_on_rule']:
                # available = delivery_id.with_context(context).read(fields=['available','price'])
                # _logger.info("-=====++++++++%r========"%(available))
            if not delivery_id.available:
                delivery_ids.remove(delivery_id.id)
        return delivery_ids
    
    
class ProductPackage(models.Model):
    _name = "product.package"
    
    @api.model
    def default_get(self,fields=None):
        res = super(ProductPackage,self).default_get(fields)
        if self._context.get('sc_sale_id'):
            res['order_id'] = self._context.get('sc_sale_id')
        return res
    
    @api.one
    @api.depends('order_id', 'packaging_id')
    def _complete_name(self):
        name = self.picking_id.name
        if self.order_id:
            name = self.order_id.name + "[%s]" % (name)
        self.complete_name = name
        
        
    @api.onchange('packaging_id')
    def _onchange_packaging_id(self):
        packaging_id= self.packaging_id
        if packaging_id:
            packaging_data = packaging_id.read(['width','height','length'])[0]
            self.width = packaging_data.get('width')
            self.length = packaging_data.get('length')
            
            
    @api.model
    def _default_uom(self):
        uom_categ_id = self.env.ref('product.product_uom_categ_kgm').id
        return self.env['product.uom'].search([('category_id', '=', uom_categ_id), ('factor', '=', 1)], limit=1)
    
    complete_name = fields.Char(compute='_complete_name',string="Package Name")
    packaging_id = fields.Many2one('product.packaging',string='Packaging',required=True)
    order_id = fields.Many2one('sale.order')
    carrier_id = fields.Many2one(related='order_id.carrier_id')
    delivery_type = fields.Selection(selection=DeliveryType)
    full_capacity  =fields.Boolean()
    cover_amount = fields.Float(string = 'Cover Amount',default=0)
    qty = fields.Float(default=0)
    weight = fields.Float(string='Weight(kg)',default=0)
    height = fields.Integer(default=1)
    width = fields.Integer(default=1)
    length = fields.Integer(default=1)
    weight_uom_id = fields.Many2one(
        'product.uom',
        string='Unit of Measure',
        readonly=True,
        help="Unit of Measure (Unit of Measure) is the unit of measurement for Weight",
        default=lambda self:self._default_uom)
    
    
    
    
    
    
    
    
    
    
    