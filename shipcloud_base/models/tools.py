# -*- coding: utf-8 -*-

from flectra import _
from flectra.exceptions import ValidationError, UserError

import logging

_logger = logging.getLogger(__name__)

def sc_convert_dimension(dimension):
    ''' Convert dimension from mm to inches '''
    return round(dimension * 0.0393701, 1)


def sc_convert_weight(weight):
    ''' Convert weight from kg to Oz '''
    return round(weight * 35.274, 1)

class SCRule(object):
    
    def __init__(self, sc_field, flectra_attr=None, convert_fun=None, required=False):
        self.sc_field = sc_field
        self.flectra_attr = flectra_attr or sc_field
        self.convert_fun = convert_fun or (lambda env, x: x)
        self.required = required
        
        
    def convert(self, rset, check_missing=False):
        def get_value():
            val = rset
            for attr in self.flectra_attr.split('.'):
                if attr == "self":
                    break
                val = getattr(val, attr)
            return val

        res = self.convert_fun(rset, get_value())
        if not res and check_missing and self.required:
            message = _('Missing value in {model_name} "{instance_name}": ' \
                        'field "{field}" is mandatory for shipping').format(model_name=rset._description,
                                                                            instance_name=rset.display_name,
                                                                            field=self.flectra_attr)
            raise UserError(message)
        return self.sc_field, res
    
    

class SCRuleSet(object):

    def __init__(self, *rules):
        self.dict_from = dict((r.sc_field, r) for r in rules)
        self.rules = self.dict_from.values()  # remove duplicates

    def convert(self, rset, check_missing=False):
        res = {}
        for name in self.dict_from:
            field, value = self.dict_from[name].convert(rset, check_missing=check_missing)
            if value:
                res[field] = value
        return res
    
    
    
    
    
    