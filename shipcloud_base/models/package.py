# -*- coding: utf-8 -*-

from flectra import api, fields, models, _

AmountOption=[
    ('fixed', 'Fixed Amount'),
    ('percentage', '%  of Product Price')
]

class ProductPackaging(models.Model):
    _inherit = 'product.packaging'
    _rec_name='name'
    
    @api.one
    @api.depends('package_carrier_type','name')
    def _complete_name(self):
        name = self.name or ''
        # if self.package_carrier_type:
        #     name +=  " [%s]" % (self.package_carrier_type)
        self.display_name = name
        
    @api.model
    def get_cover_amount(self,amount):
        if self.cover_amount_option == 'fixed':
            return self.cover_amount
        return amount* self.cover_amount / 100
    
    
    cover_amount_option = fields.Selection(selection = AmountOption,default ='percentage',required=1)
    cover_amount = fields.Integer(string='Cover Amount',default =80,help="""This is the declared value/cover amount for an individual package.""")
    display_name = fields.Char(compute='_complete_name',string="Complete Name")
    height = fields.Float(string='Height',default=1,help='Measure the longest side of the package, DimensionUnit will be CM for API-UoM [KG] and IN for API-UoM [LB]',)
    width = fields.Float(string='Width',default=1,help='DimensionUnit will be CM for API-UoM[KG] and IN for API-UoM [LB]')
    length = fields.Float(string='Length',default=1,help='DimensionUnit will be CM for API-UoM[KG] and IN for API-UoM [LB]')
    max_weight = fields.Float(string='Max Weight',help='Maximum weight (in kg) can be shipped in this packaging',default=65)
    product_tmpl_id =  fields.Many2one('product.template',string='Template',required=False)
    product_tmpl_ids = fields.Many2many('product.template','product_tmp_product_packaging_rel','packaging_id','product_tmpl_id',string='Template')
    
    _sql_constraints = [
        ('positive_max_weight',
         'CHECK(max_weight>0.0)',
         'Max Weight must be positive (max_weight>0.0).'),
    ]
    
    
    
    
    