# -*- coding: utf-8 -*-
# Part of flectra. See LICENSE file for full copyright and licensing details.

{
    'name': 'ShipCloud Delivery (base module)',
    'category': 'Delivery',
    'author': "eComBucket",
    'version': '0.1',
    'maintainer': 'support@ecombucket.zohosupport.com',
    'website':'https://twitter.com/ecombucket',
    "depends": ["website_sale_delivery"],
    'description': """
    """,
    'data': [
             'views/carrier.xml',
             'views/order.xml',
             'views/package.xml',
             'views/picking.xml',
             'views/quant.xml',
            'views/res_partner.xml',
             
             'data/data.xml',
             'security/ir.model.access.csv',
             ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
