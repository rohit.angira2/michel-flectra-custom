# -*- coding: utf-8 -*-
{
    "name": "ShipCloud Delivery DHL Express",
    'category': 'Delivery',
    'author': "eComBucket",
    'version': '11.0.1',
    'maintainer': 'support@ecombucket.zohosupport.com',
    'website':'https://twitter.com/ecombucket',
    "depends": ["shipcloud_base"],
    "description": """
        """,
    'demo': [],
    'data': [
        'data/packaging.xml',
        'data/carrier.xml',
        
#         'views/carrier.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
