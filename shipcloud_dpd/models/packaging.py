# -*- coding: utf-8 -*-

from flectra import fields, models

class Packaging(models.Model):
    _inherit = 'product.packaging'

    package_carrier_type = fields.Selection(selection_add=[('sc_dpd', 'DPD (ShipCloud)')])
