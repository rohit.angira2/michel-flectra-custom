# -*- coding: utf-8 -*-
# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

from flectra import models


class ProductBrand(models.Model):
    _name = 'product.brand'
    _inherit = ['product.brand', 'carousel.slider']
