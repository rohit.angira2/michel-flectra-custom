# -*- coding: utf-8 -*-

from flectra import models, fields

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    is_packstation = fields.Boolean('Is Packstation?')
    
    
    def _get_delivery_methods(self):
        address = self.partner_shipping_id
        website = self.env['website'].get_current_website()
        if self.is_packstation:
            return self.env['delivery.carrier'].sudo().search([('website_published', '=', True), ('website_ids', 'in', website.id),('is_packstation_carrier','=',True)]).available_carriers(address)
        return self.env['delivery.carrier'].sudo().search([('website_published', '=', True), ('website_ids', 'in', website.id)]).available_carriers(address)