# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import fields, models


class DeliveryCarrier(models.Model):
    _inherit = 'delivery.carrier'
    
    is_packstation_carrier = fields.Boolean('Is Packstation Carrier?')