# -*- coding: utf-8 -*-
# Part of flectra. See LICENSE file for full copyright and licensing details.

{
    'name': 'Website Extends',
    'category': 'Website',
    'version': '0.1',
    "depends": ["website_sale",'website_sale_delivery','shipcloud_base'],
    'description': """
    """,
    'data': [
             'views/templates.xml',
             'views/delivery_carrier.xml',
             ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
