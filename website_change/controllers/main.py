# -*- coding: utf-8 -*-
from flectra import http, _
from flectra.http import request
from werkzeug.exceptions import Forbidden
from flectra.addons.website_sale.controllers.main import WebsiteSale

class WebsiteSaleCustom(WebsiteSale):
    
    @http.route(['/shop/address'], type='http', methods=['GET', 'POST'], auth="public", website=True)
    def address(self, **kw):
        Partner = request.env['res.partner'].with_context(show_address=1).sudo()
        order = request.website.sale_get_order()
        
        #Add by Varun - Packsation only DHL delivery
        if kw.get('packstation_checkbox',False):
            order.write({'is_packstation':True})

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        mode = (False, False)
        def_country_id = order.partner_id.country_id
        values, errors = {}, {}

        partner_id = int(kw.get('partner_id', -1))

        # IF PUBLIC ORDER
        if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
            mode = ('new', 'billing')
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                def_country_id = request.env['res.country'].search([('code', '=', country_code)], limit=1)
            else:
                def_country_id = request.website.user_id.sudo().country_id
        # IF ORDER LINKED TO A PARTNER
        else:
            if partner_id > 0:
                if partner_id == order.partner_id.id:
                    mode = ('edit', 'billing')
                else:
                    shippings = Partner.search([('id', 'child_of', order.partner_id.commercial_partner_id.ids)])
                    if partner_id in shippings.mapped('id'):
                        mode = ('edit', 'shipping')
                    else:
                        return Forbidden()
                if mode:
                    values = Partner.browse(partner_id)
            elif partner_id == -1:
                mode = ('new', 'shipping')
            else: # no mode - refresh without post?
                return request.redirect('/shop/checkout')

        # IF POSTED
        if 'submitted' in kw:
            if kw.get('packstation_checkbox',False):
                old_required_field = kw.get('field_required')
                kw.update({'field_required':old_required_field + ',street2'})
            pre_values = self.values_preprocess(order, mode, kw)
            if not pre_values.get('name',False):
                pre_values['name'] = pre_values.get('first_name','') + " " + pre_values.get('last_name','')
            errors, error_msg = self.checkout_form_validate(mode, kw, pre_values)
            post, errors, error_msg = self.values_postprocess(order, mode, pre_values, errors, error_msg)
            
            if errors:
                errors['error_message'] = error_msg
                values = kw
            else:
                post.update({'first_name':kw.get('first_name',''),'last_name':kw.get('last_name',''),'street_name':kw.get('street',''),'street_number':kw.get('street_number','')})
                partner_id = self._checkout_form_save(mode, post, kw)
                if kw.get('packstation_checkbox',False):
                    partner = request.env['res.partner'].sudo().browse(partner_id)
                    partner.sudo().write({'is_packstation_address':True})
                
                if mode[1] == 'billing':
                    order.partner_id = partner_id
                    order.onchange_partner_id()
                elif mode[1] == 'shipping':
                    order.partner_shipping_id = partner_id

                order.message_partner_ids = [(4, partner_id), (3, request.website.partner_id.id)]
                if not errors:
                    return request.redirect(kw.get('callback') or '/shop/checkout')

        country = 'country_id' in values and values['country_id'] != '' and request.env['res.country'].browse(int(values['country_id']))
        country = country and country.exists() or def_country_id
        render_values = {
            'website_sale_order': order,
            'partner_id': partner_id,
            'mode': mode,
            'checkout': values,
            'country': country,
            'countries': country.get_website_sale_countries(mode=mode[1]),
            "states": country.get_website_sale_states(mode=mode[1]),
            'error': errors,
            'callback': kw.get('callback'),
        }
        return request.render("website_sale.address", render_values)
