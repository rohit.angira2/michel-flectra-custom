# -*- coding: utf-8 -*-
# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

from flectra import models


class ProductPublicCategory(models.Model):
    _name = 'product.public.category'
    _inherit = ['product.public.category', 'carousel.slider']
