# -*- coding: utf-8 -*-
# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Theme Aliment',
    'description': 'Theme Aliment is theme developed by'
                   ' 73Lines Development Team '
                   'consist of 17  snippets to build a '
                   'beautiful website with 6 '
                   'color variations, 6 font options, 2 '
                   'Layouts and 8 Patterns.',
    'category': 'Theme/Ecommerce',
    'version': '1.0',
    'author': '73Lines',
    'depends': [

        # Default Modules

        'website',
        'website_mass_mailing',
        'website_blog',
        'website_sale_comparison',

        # 73lines Depend Modules

        # Don't forget to see README file in order to how to install
        # In order to install complete theme, uncomment the following .
        # Dependent modules are supplied in a zip file along with the theme,
        # if you have not received it,please contact us at enquiry@73lines.com
        #  with proof of your purchase.
        ###############################################################

        'carousel_slider_73lines',
        'snippet_blog_carousel_73lines',
        'snippet_product_brand_carousel_73lines',
        'snippet_product_carousel_73lines',
        'snippet_product_category_carousel_73lines',
        'website_product_categ_menu_and_banner_73lines',
        'website_language_flag_73lines',
        'website_mega_menu_73lines',

        ###############################################################

    ],
    'data': [

        # Snippets

        'snippets/s_state_counter.xml',
        'snippets/s_parallax_two.xml',
        'snippets/s_parallax_one.xml',
        'snippets/s_footer_one.xml',
        'snippets/s_aliment_banner.xml',
        'snippets/s_our_menu.xml',
        'snippets/s_our_usp.xml',

        'views/assets.xml',
        'views/home_page_carousel.xml',
        'views/home_page_mini_carousel.xml',
        'views/products_template.xml',
        'views/single_product_template.xml',
        'views/product_collapse_categories_template.xml',
        'views/navbar_template.xml',
        'views/rating_template.xml',
        'views/customize_model.xml',
        'views/homepage_cate_carousel_template.xml',
    ],
    'demo': [
        'demo_pages/homepage.xml',
        'demo_pages/menu_data.xml',
        'demo_pages/footer_template.xml',
        'demo_pages/blog_post_demo.xml',
        'demo_pages/brand_demo.xml',
    ],
    'images': [
        'static/description/aliment-banner.png',
    ],
    'price': 99,
    'license': 'FPL-1',
    'currency': 'EUR',
    'live_test_url': 'https://www.73lines.com/r/l2S'
}
