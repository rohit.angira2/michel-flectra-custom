# -*- coding: utf-8 -*-

{
    'name': 'Website Email on Contact-Us form submit',
    'category': 'Website',
    'version': '1.0.0',
    'author': "Simbeez",
    'depends': ['website_crm'],
    'summary': """Receive an e-mail when website vistors fill in data on your company's contact form.""",
    'description': """
Website Email from Contact-Us form submit

==========================

Receive an e-mail when website vistors fill in data on your company's contact form (adds an e-mail template which is used as a notification).
    """,
    'data': [
        'data/website_contact_notify_data.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
