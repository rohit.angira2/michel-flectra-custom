# -*- coding: utf-8 -*-

import flectra


class WebsiteForm(flectra.addons.website_form.controllers.main.WebsiteForm):

    def insert_record(self, request, model, values, custom, meta=None):
        record_id = super(WebsiteForm, self).insert_record(request, model, values, custom, meta)

        if model.model == 'crm.lead':
            template = request.env.ref(
                'website_contact_notify.website_contact_notify_mail', False)
            if template:
                mail_id = template.sudo().send_mail(record_id, force_send=True)

        return record_id
