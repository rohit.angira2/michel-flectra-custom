# -*- coding: utf-8 -*-
{
    "name": "ShipCloud Delivery iloxx",
    'category': 'Delivery',
    'version': '0.1',
    "depends": ["shipcloud_base", "product"],
    "description": """
        """,
    'demo': [],
    'data': [
        'data/packaging.xml',
        'data/carrier.xml',
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
