# -*- coding: utf-8 -*-

from flectra import models, fields

class iloxxShipCloudCarrier(models.Model):
    _inherit = 'delivery.carrier'

    delivery_type = fields.Selection(selection_add=[('sc_iloxx', "iloxx (MyDPD Business)")])

    def sc_iloxx_send_shipping(self, pickings):
        return self.sc_send_shipping(pickings)

    def sc_iloxx_get_tracking_link(self, pickings):
        return self.sc_get_tracking_link(pickings)

    def sc_iloxx_get_shipping_price_from_so(self, orders):
        return self.sc_get_shipping_price_from_so(orders)
    
    def sc_iloxx_rate_shipment(self, order):
        carrier = self._match_address(order.partner_shipping_id)
        if not carrier:
            return {'success': False,
                    'price': 0.0,
                    'error_message': _('Error: this delivery method is not available for this address.'),
                    'warning_message': False}
        price = self.sc_get_shipping_price_from_so(order)
        return {'success': True,
                'price': price,
                'error_message': False,
                'warning_message': False}
    