# -*- coding: utf-8 -*-
# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

from . import models
from . import controllers
