# -*- coding: utf-8 -*-

{
    'name': 'Delivery Report Changes',
    'version': '11.0',
    'description': ''' Changes in Report Layout
    ''',
    'author': 'Simbeez',
    'website': 'www.simbeez.com',
    'depends': [
        'stock'
    ],
    'data': [
            'views/deliveryslip_report_changes.xml',
        ],
    'application': False,
    'installable': True,
    'currency': 'USD',
    'license': 'AGPL-3',
}
